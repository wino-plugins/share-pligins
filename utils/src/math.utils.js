function generateRandomNumber(length) {
   let num = ''

   for (i = 0; i < length; i++) {
      num += Math.round(Math.random() * 9)
   }

   return num
}

module.exports = {
   generateRandomNumber
}