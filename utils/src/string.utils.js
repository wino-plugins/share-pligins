function generateRandomString(length) {
   let str = ''
   let chars = [
      '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
   ]

   for (let i = 0; i < length; i++) {
      str += chars[Math.round(Math.random() * 61)]
   }

   return str;
}

function template(msg, data) {
   return msg.replace(
      /{(\w*)}/g, // or /{(\w*)}/g for "{this} instead of %this%"
      function (m, key) {
         if (data) {
            return data.hasOwnProperty(key) ? data[key] : "";
         }

         return data
      }
   );
}

module.exports = {
   generateRandomString,
   template,
}