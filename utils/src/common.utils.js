function sleep(seconds = 1){
   return new Promise((resolve,reject) => {
      try {
         setTimeout(() => {
            resolve(true)
         }, seconds * 1000);

      } catch (err) {
         reject(err)
      }
   })
}

module.exports = {
   sleep,
}