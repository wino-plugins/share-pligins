module.exports = {
   StringUtils: require('./src/string.utils'),
   MathUtils: require('./src/math.utils'),
   APIUtils: require('./src/api.utils'),
   FileUtils: require('./src/file.utils'),
   CommonUtils: require('./src/common.utils'),
}