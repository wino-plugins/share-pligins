const fs = require('fs')
const ejs = require('ejs')

function readFile(name){
   return new Promise((resolve,reject) => {
      try {
         fs.readFile(name, function read(err, data) {
            if (err) {
               throw err;
            }
            
            resolve(data)
         });
      } catch (err) {
         reject(err)
      }
   })
}

async function getMailContent(template,template_data){
   try {
      let data = await readFile(`${rootDir}/templates/notification/${template}.ejs`)
      data = data.toString()

      return await ejs.render(data, template_data,{
         rmWhitespace: true,
         async: true
      });
   } catch (err) {
      throw err
   }
}

module.exports = {
   getMailContent
}