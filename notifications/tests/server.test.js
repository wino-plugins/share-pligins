const RabbitMQ = require('wino-plug-rabbitmq')
const Notification = require('../index')
const path = require('path')
global.chalk = require('chalk')

global.rootDir = __dirname
global.configs = {
   service: {
      serviceName: 'TestPluginNotification',
   }
}

async function startServices(){
   try {
      configs['RabbitMQ'] = RabbitMQ.configs
      await RabbitMQ.init()

      // Notification.sendNotification({
      //    channel: 'EMAIL',
      //    to: ['geoffreykariithi@gmail.com'],
      //    message: 'Hello World. This is a new message',
      //    data: {
      //       subject: "This is a subject"
      //    }
      // });

      await Notification.sendNotificationTemplate({
         channel: 'EMAIL',
         to: ['geoffreykariithi@gmail.com'],
         data: {
            subject: "This is a subject"
         },
         template: 'email/new-registration',
         template_data: {user: {
            name: 'Geoffrey Kariithi',
            email: 'geoffreykariithi@gmail.com'
         }}
      })
   } catch (err) {
      await RabbitMQ.exit()
      throw err
   }
}

startServices()