const mail = require('./mail')

function publishMessage(data) {
   try{
      QueueChannel.sendToQueue(data.exchange, Buffer.from(JSON.stringify(data.payload)), data.options);
   }catch(err){
      throw err
   }
}

function sendNotification({channel,to,message,data}){
   try {
      console.log('[x] Sending notification')

      publishMessage({
         exchange: 'notification:send',
         payload: {
            to,
            channel,
            service: configs.service.serviceName,
            data,
            message
         },
         options: {},
      })
   } catch (err) {
      throw err
   }
}

async function sendNotificationTemplate({channel,to,data,template_data,template}){
   try {
      console.log('[x] Sending notification')

      if(channel === 'EMAIL'){
         publishMessage({
            exchange: 'notification:send',
            payload: {
               to,
               channel,
               service: configs.service.serviceName,
               data,
               type: 'html',
               message: await mail.getMailContent(template,template_data)
            },
            options: {},
         })
      }

      return true
   } catch (err) {
      throw err
   }
}

module.exports = {
   sendNotification,
   sendNotificationTemplate
}