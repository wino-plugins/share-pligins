const MongoDB = require('./MongoDB')

const configs = {
   has_auth: process.env.DATABASE_HAS_AUTH || false,
   db: process.env.DATABASE_NAME || 'sample_db',
   user: process.env.DATABASE_USERNAME || 'sample',
   pass: process.env.DATABASE_PASSWORD || 'sample123!',
   host: process.env.DATABASE_HOST || '127.0.0.1',
   port: process.env.DATABASE_PORT || '27017',
   repl: process.env.DATABASE_REPLS || 'rs0',
   authDB: process.env.AUTHENTICATING_DB || 'admin',
}

async function init(){
   try {
      await MongoDB.connect()
   } catch (err) {
      throw err
   }
}

async function exit(){
   try {
      await MongoDB.disconnect()
   } catch (err) {
      throw err
   }
}

module.exports = {
   configs,
   init,
   exit
}