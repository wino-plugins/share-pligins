global.Mongoose = require('mongoose');
const dbOptions = {
   // autoIndex: false, // Don't build indexes
   useNewUrlParser: true,
   useCreateIndex: true,
   useFindAndModify: false,
   // reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
   // reconnectInterval: 500, // Reconnect every 500ms
   poolSize: 10, // Maintain up to 10 socket connections
   bufferMaxEntries: 0, // If not connected, return errors immediately rather than waiting for reconnect
   useUnifiedTopology: true, // Server Discovery and Monitoring engine
   connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
   socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
   family: 4 // Use IPv4, skip trying IPv6
};
let dbConnection = null, isClosed = false;

function getMongoURL(){
   let connectionString = '';

   if (configs.mongo.has_auth == true) {
      connectionString = `mongodb://${configs.mongo.user}:${configs.mongo.pass}@${configs.mongo.host}:${configs.mongo.port}/${configs.mongo.db}?authMechanism=DEFAULT`;
   } else {
      connectionString = `mongodb://${configs.mongo.host}:${configs.mongo.port}/${configs.mongo.db}`;
   }

   return connectionString;
}

function connect(){
   return new Promise((resolve,reject)=> {
      try {
         let connectionURL = getMongoURL();
         Mongoose.connect(connectionURL, dbOptions, async (err, db) => {
            if (err) {
               console.error(chalk.red(`❌ Error connecting to MongoDB: ${connectionURL}`));
               reject(err);
            } else {
               console.log(chalk.cyan("✔️ Successfully connected to MongoDB"));
               dbConnection = db;
               resolve(true)
            }
         })
      } catch (err) {reject(err);}
   })
}

function disconnect(){
   if(!isClosed && dbConnection != null){
      dbConnection.close()
      isClosed = true
      console.log(chalk.magenta('✔️ Successfully closed MongoDB connection'))
   }
}

module.exports = {
   connect,
   disconnect
}