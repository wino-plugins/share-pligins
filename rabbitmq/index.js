let configs = {
   port: process.env.QUEUE_PORT || '5672',
   user: process.env.QUEUE_USER || 'guest',
   pass: process.env.QUEUE_PASS || 'guest',
   host: process.env.QUEUE_HOST || '127.0.0.1',
   queueType: process.env.QUEUE_TYPE || 'direct',
   vhost: process.env.QUEUE_VHOST || null,
}, Rabbit = require('./RabbitMQ')

async function init() {
   try {
      await Rabbit.connect()
   } catch (err) {
      throw err
   }
}

async function exit() {
   try {
      await Rabbit.disconnect()
   } catch (err) {
      throw err
   }
}

module.exports = {
   configs,
   init,
   exit
}