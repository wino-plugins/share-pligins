## Required Packages
This plugin requires the following packages
   - Mongoose

## Installation
Run the following command to install the necessary dependencies

```bash
npm install --save mongoose
```