const amqp = require('amqplib/callback_api');
const options ={};

let isClosed = false, queueConnection = null;

function getURL() {
   if(configs.rabbitmq.vhost){
      return `amqp://${configs.rabbitmq.user}:${configs.rabbitmq.pass}@${configs.rabbitmq.host}${configs.rabbitmq.vhost}`;
   }else{
      return `amqp://${configs.rabbitmq.user}:${configs.rabbitmq.pass}@${configs.rabbitmq.host}`;
   }
}

function connect(){
   return new Promise((resolve, reject) => {
      try {
         let connectionURL = getURL();
         amqp.connect(connectionURL, function (err, connection) {
            if (err) {
               console.error(chalk.red(`❌ Error connecting to RabbitMQ: ${connectionURL}`));
               reject(err);
            } else {
               console.log(chalk.cyan("✔️ Successfully Connected to RabbitMQ"));
               queueConnection = connection;

               queueConnection.createChannel(function (err, channel) {
                  if (err) {
                     console.error(chalk.red(`❌ Error Creating RabbitMQ Channel: ${connectionURL}`));
                     reject(err)
                  } else {
                     console.log(chalk.cyan("✔️ RabbitMQ channel created"));
                     global.QueueChannel = channel
                     resolve(true)
                  }
               })
            }
         });
      } catch (err) { reject(err); }
   });
}

function disconnect(){
   if (!isClosed && queueConnection != null) {
      queueConnection.close()
      isClosed = true
      console.log(chalk.magenta('✔️ Successfully closed RabbitMQ connection'))
   }
}

module.exports = {
   connect,
   disconnect,
}