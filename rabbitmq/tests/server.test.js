const RabbitMQ = require('../index')
global.configs = {}
global.chalk = require('chalk')

async function startServices(){
   try {
      configs['RabbitMQ'] = RabbitMQ.configs
      await RabbitMQ.init()
   } catch (err) {
      await RabbitMQ.exit()
      throw err
   }
}

startServices()