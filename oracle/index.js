const Oracle = require('./src/oracle')

const configs = {
   db: process.env.DATABASE_NAME || 'sample_db',
}

async function init(){
   try {
      await Oracle.connectToDbs()
   } catch (err) {
      throw err
   }
}

async function exit(){
   try {
      
   } catch (err) {
      throw err
   }
}

module.exports = {
   configs,
   init,
   exit
}