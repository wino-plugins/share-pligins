const {Mega} = require('../src/mega')
const fs = require('fs');


async function uploadMultipleFiles(){
   try {
      let dir_name = `C:\\Users\\hp\\Pictures\\wallpaper`, max_number_of_uploads = 2
      let mega = new Mega('geoffreykariithi@gmail.com','Kimani003!')
      let storage = await mega.logIn()

      fs.readdir(dir_name, async (err, files) => {
         // files.forEach(file => {
         //    console.log(file);
         // });

         let folder = await mega.mkdir(mega.getFileFromName('Cloud Drive'),'Cool Wallpapers')
         for (let i = 0; i < max_number_of_uploads; i++) {
            mega.upload(folder,`${dir_name}\\${files[0]}`,files[0])
            console.log(`Uploading: ${files[0].substring(0,10)}`)
            files.splice(0,1)
         }

         mega.event.on('upload:started',(data) => {
            console.log(`Uploading: ${data.name.substring(0,10)}`)
         })
         mega.event.on('upload:progress',(data) => {
            // console.log(`Uploading: ${data.name} - ${data.progress}%`)
         })
         mega.event.on('upload:complete',(data) => {
            console.log(`Upload Complete: ${data.name.substring(0,10)}, Remainder: ${files.length}`)

            if(files.length > 0){
               mega.upload(folder,`${dir_name}\\${files[0]}`,files[0])
               files.splice(0,1)
            }else{
               console.log('Totally Complete')
            }
         })
      });
   } catch (err) {
      throw err
   }
}

uploadMultipleFiles()